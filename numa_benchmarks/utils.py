# Copyright 2022 California Institute of Technology
#
# You should have received a copy of the licensing terms for this
# software included in the file “LICENSE” located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE.txt

from . import NodeCPUManager
import time

subdir_prefix="workdir"

def add_numa_options(parser):
    parser.add_argument("--executable", type=str,
                        help="Full path to executable to run")
    parser.add_argument("--exec-args", type=str, nargs='*',
                        help="Arguments to pass to each invocation"
                        " of the executable", default=[])
    parser.add_argument("--subdir-prefix", type=str,
                        help="Beginning of name of subdirectory for"
                        " each invocation; '_job<id>' will be appended",
                        default=subdir_prefix)
    parser.add_argument("--outfile-prefix", type=str,
                        help="Beginning of name of file to hold stdout and"
                        " stderr within workdir of each invocation;"
                        " '_job<id>' will be appended",
                        default="stdout_stderr")
    parser.add_argument("--cpus-per-job", type=int,
                        help="Number of cpu cores each job invocation"
                        " will use. Job is responsible for using requested"
                        " number of cpus", default=1)
    parser.add_argument("--poll-interval", type=int,
                        help="Time in seconds between polls of the managed"
                        " processes to see whether any are still running."
                        " The program does this infrequently to avoid busy"
                        " loops that might interfere with the benchmark",
                        default=60)
    parser.add_argument("--allow-split-nodes", action="store_true",
                        help="Whether or not to allow the cpus allocated"
                        " to each job to split across NUMA nodes. Specifying"
                        " this option could cause a performance penalty,"
                        " but may also decrease cpu underutilization")
    parser.add_argument("--forbid-underutilize", action="store_true",
                        help="Raise an exception if there are any"
                        " unassigned cpus once all jobs have been"
                        " assigned. Otherwise a warning is printed"
                        " instead")
    parser.add_argument("--only-local-mem", action="store_true",
                        help="Require each subprocess to only allocate"
                        " memory from its local node (in addition to"
                        " binding it to the allocated cpus). Only avilable"
                        " when 'numactl' is the manager_exec")
    parser.add_argument("--manager-exec", type=str,
                        choices=['numactl', 'taskset'],
                        help="Underlying executable to handle binding each"
                        " process to its allocated CPUS. The corresponding"
                        " system command must be installed")
    parser.add_argument("--verbose", action="store_true",
                        help="Run in verbose mode. Currently the only"
                        " effect is to print out the parsed options"
                        " whenever 'from_cli' is called")

def verify_numa_options(opts, parser):
    if opts.executable is None:
        parser.error("You must specify an executable")
    if not opts.executable.startswith("/"):
        parser.error("You must provde the full path the"
                     " executable".format(opts.executable))
    if opts.manager_exec != 'numactl':
        if opts.only_local_mem:
            parser.error("You can only specify --only-local-mem"
                         " when using the numactl manager."
                         " You specified {0}.".format(opts.manager_exec))

def from_cli(opts):
    if opts.verbose:
        print("Verbose mode: parsed options are {0}".format(opts))

    manager = NodeCPUManager(allow_split_nodes=opts.allow_split_nodes,
                             manager_exec=opts.manager_exec)
    return manager

def handle_unassigned(opts, manager):
    if manager.underutilized:
        if opts.forbid_underutilize:
            raise RuntimeError("You forbid underutilization but these"
                               " cpus are unassigned: {0}".format(manager.unassigned))
        else:
            print("Warning: the machine may be underutilized as the"
                  " following cpus are unassigned:"
                  " {0}".format(manager.unassigned))

    return

def watch_procs(opts, procs):
    while any(p.poll() is None for p in procs):
        time.sleep(opts.poll_interval)

    for p in procs:
        if p.poll() != 0:
            raise RuntimeError("Subprocess {0} returned non-zero"
                               " exit code {1}".format(p.pid,
                                                       p.returncode))

    return
