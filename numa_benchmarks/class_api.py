# Copyright 2022 California Institute of Technology
#
# You should have received a copy of the licensing terms for this
# software included in the file “LICENSE” located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE.txt

import subprocess, shutil, os

def _cpu_list_per_job(ncpus, nstart, cpulist, njobs):
    """
    Helper function that takes a number of cpus per job,
    a starting job number, a list of cpu numbers, and a
    number of jobs. It will return a dict keyed by job
    number, of which the first will be nstart, and for
    each key in the dict the value is a string of comma
    separated cpu numbers, each of length ncpus. If njobs
    is not 'None', then the total number of assigned jobs 
    added to the dict will be less than or equal to njobs;
    if it is 'None', then as many jobs as possible will be
    assigned. 

    Whenever cpus are assigned to a job, they are deleted
    from the front of 'cpulist' (which is therefore modified
    in-place).
    """
    rdict = {}
    n = nstart
    i = 0
    while (ncpus <= len(cpulist)):
        if (njobs is not None) and (i >= njobs):
            break
        l = cpulist[0:ncpus]
        if len(l) == ncpus:
            rdict.update({n:','.join(map(str,l))})
            del(cpulist[0:ncpus])
            i += 1
            n +=1
        else:
            # We couldn't assign the required number of cpus, and
            # on the next attempt on the loop, we will have
            # len(cpulist) < ncpus
            pass
    return rdict


class NodeCPUManager(object):
    """
    A class to facilitate querying the NUMA nodes and corresponging cores
    of a CPU, and to construct lists of cpu suitable for binding
    tasks while respecting (as needed) node layout.

    This is still very rudimentary; we only rely on 'lscpu' for node and
    cpu information, and do not consider any levels of cache.

    There are also methods for setting up mappings between cpus and jobs,
    where each job requires the same number of cpus. The intended use case
    is fully loading a machine for either benchmarking or other situations
    where all jobs have identical memory and cpu requirements.

    There is also a method for starting and managing such jobs via Python's
    subprocess module.  This method can be called on a subset of jobs,
    allowing some parameters (such as the executable or the need for output
    files) to vary (for example, if some of the jobs are just 'busy work'
    to ensure the machine is fully loaded).

    This is not a singleton class, and will not be aware of other instances.
    Callers are responsible for ensuring that other instances are not used
    to manage the same cpus until any processes managed through one instance
    are finished, and the instance destroyed.
    """
    def __init__(self, input_stream=None, allow_split_nodes=True,
                 manager_exec='numactl'):
        """
        Setup the class: using either the provided input, or querying
        lscpu if none is provided, we create a dictionary whose keys
        are unique nodes, and for each node in the dictionary, we attach
        a list of the cpus that belong to that node.

        Parameters:
        ==============
        input_stream: long string (optional)
            If provided, should match the output of 'lscpu -p=node,cpu'.
            Lines beginning with '#' are ignored. If omitted, lscpu will
            be called instead.

        allow_split_nodes: bool (default True)
            If True, then when calling 'update_job_map', it will be
            permitted to have lists that span different nodes. This could
            cause performance problems. If False, then only lists that
            are entirely contained within the cpus of one node will be
            returned. This could lead to underutilization.

        manager_exec: 'numactl' (default) or 'taskset'
            Which of the two supported executables should be invoked in
            later calls to the 'create_procs' method to actually bind
            the created processes to the cpus assigned to their job.
            An OSError is raised if the indicated executable cannot be
            found or is not executable. Note that only 'numactl' may
            be used if the 'only_local_mem' option will be set whenever
            'create_procs' is called.
        """

        if input_stream is None:
            execpath = shutil.which('lscpu')
            if execpath is not None:
                p = subprocess.run([execpath,'-p=node,cpu'], check=True,
                                   capture_output=True)
                input_stream = p.stdout.decode()
                del p
            else:
                raise OSError("Could not locate required executable"
                              " 'lscpu'. Make sure it is installed"
                              " on your system.")

        self._allow_split_nodes_ = allow_split_nodes
        allowed_managers = ['numactl', 'taskset']
        if manager_exec not in allowed_managers:
            raise ValueError("'manager_exec' of {0} is not supported."
                             " Valid choices are {1}".format(manager_exec,
                                                             allowed_managers))

        self._manager_exec_path = shutil.which(manager_exec)
        if self._manager_exec_path is None:
            raise OSError("'manager_exec' choice not found; check that"
                          " {0} is installed and executable".format(manager_exec))
        self._manager_exec = manager_exec
        if manager_exec == 'numactl':
            self._manager_arg = '-C '
        if manager_exec == 'taskset':
            self._manager_arg = '-c '
        ndict = {}
        for line in input_stream.splitlines():
            if not line.startswith('#'):
                node, cpu = line.split(',')
                n = int(node)
                c = int(cpu)
                if n in ndict:
                    ndict[n].append(c)
                else:
                    ndict.update({n: [c]})

        for k in ndict.keys():
            l = sorted(ndict[k])
            ndict[k] = l

        self._node_cpu_dict_ = dict(sorted(ndict.items(), 
                                           key = lambda item: item[0]))
        del ndict
        self._all_cpus_ = []
        # To check that no cpu is repeated, we have to actually
        # iterate over each element in the cpu lists
        for node in self._node_cpu_dict_:
            for c in self._node_cpu_dict_[node]:
                if c not in self._all_cpus_:
                    self._all_cpus_.append(c)
                else:
                    raise RuntimeError("CPU {0} is assigned to more"
                                       " than one node".format(c))

        self._node_count_dict_ = dict([(k, len(v)) for k, v in 
                                       self._node_cpu_dict_.items()])
        self._cpu_counts_ = [v for k, v in self._node_count_dict_.items()]
        self.nnodes = len(self._cpu_counts_)
        self.maxcpus = max(self._cpu_counts_)
        self.mincpus = min(self._cpu_counts_)
        self.ncpus = sum(self._cpu_counts_)
        self._job_map_ = {}
        if self._allow_split_nodes_:
            self._avail_node_dict_ = {0: self._all_cpus_.copy()}
        else:
            self._avail_node_dict_ = self._node_cpu_dict_.copy()

        return
        
    def get_allows_split_nodes(self):
        return self._allow_split_nodes_

    def set_allows_split_nodes(self, value):
        raise RuntimeError("allow_split_nodes can only be set on"
                           " instantiation")

    def del_allows_split_nodes(self):
        raise RuntimeError("allow_split_nodes can only be changed"
                           " on instantiation")

    allows_split_nodes = property(get_allows_split_nodes,
                                  set_allows_split_nodes,
                                  del_allows_split_nodes)

    def get_unassigned(self):
        l = []
        for k, v in self._avail_node_dict_.items():
            l += v
        return l

    def set_unassigned(self, value):
        raise RuntimeError("You can only change unassigned cpus by"
                           " updating the job map")

    def del_unassigned(self):
        raise RuntimeError("You cannot delete the unassigned cpus directly")

    unassigned = property(get_unassigned, set_unassigned, del_unassigned)

    def get_job_map(self):
        """
        Get the mapping created by all previous calls to
        'update_bind_map'.  Note that all of the job ids in list form may
        be accessed via the 'jobs' attribute, and the unassigned cpus
        (if any) by the 'unassigned' attribute.


        Returns:
        ========
        job_dict: dict
            A dictionary whose keys are integral jobs numbers, beginning
            at zero, and whose corresponding values are strings
            consisting of comma-separated lists of unique cpu numbers,
            each of length cpus_per_job. This dict will be empty if
            there was no way to make such an assignment on the available
            resource while respecting both cpus_per_job and the value
            of allow_split_nodes during all previous calls of 
            'update_job_map', or if it has never been called, since
            class instantiation, or the las call to 'clear_job_map'.
        """
        return self._job_map_

    def set_job_map(self, value):
        raise RuntimeError("You can only change the job map through"
                           " the 'update_job_map' method")

    def del_job_map(self):
        self.clear_job_map()

    job_map = property(get_job_map, set_job_map, del_job_map)

    def clear_job_map(self):
        del self._job_map_
        self._job_map_ = {}
        if self.allows_split_nodes:
            self._avail_node_dict_ = {0: self._all_cpus_.copy()}
        else:
            self._avail_node_dict_ = self._node_cpu_dict_.copy()

    def update_job_map(self, cpus_per_job=1, num_jobs=None):
        """
        This method updates the mapping between jobs and assigned cpu cores. 
        The caller may specify the number of cpus per job, and the total
        number of jobs to attempt creating with this assignment.

        The current mapping can be queried via the 'job_map' attribute,
        and will be used when calling 'create_procs'. The list of available
        jobs itself is the 'jobs' attribute.

        Parameters:
        ===========

        cpus_per_job: int (default 1)
            The number of CPUS each job requires

        num_jobs: int or None (default None)
            The number of jobs of this number of cpus to create. If this
            number is greater than can be satisfied on the available
            resource while also respecting 'allows_split_nodes', then a
            ValueError is raised. If the default value of 'None' is
            provided, then as many jobs of this size as will fit on
            the remaining unassigned cpus are created. In this case a
            ValueError is only raised if that number is zero.

        Returns:
        ========
        job_list: list of ints
            A list of unique integers that are the job ids (as used in
            the keys of 'job_map') corresponding to the jobs created by
            this particular call to 'update_job_map.'
        """

        ncpus = cpus_per_job
        njobs = num_jobs
        # We start at the next available job number:
        if len(self._job_map_) == 0:
            nstart = 0
        else:
            nstart = sorted([k for k in self._job_map_.keys()])[-1] + 1

        dkeys = []
        newjobs = []
        for node, cpus in self._avail_node_dict_.items():
            if (njobs is not None) and (len(newjobs) >= njobs):
                break
            jdict = _cpu_list_per_job(ncpus, nstart, cpus, njobs)
            l = [k for k in jdict.keys()]
            newjobs += l
            nstart += len(l)
            if len(cpus) == 0:
                dkeys.append(node)
            if njobs is not None:
                njobs -= len(l)
            self._job_map_.update(jdict)

        if len(dkeys) > 0:
            for node in dkeys:
                del(self._avail_node_dict_[node])

        if num_jobs is None:
            if len(newjobs) == 0:
                raise ValueError("You requested assigning to all"
                                 " remaining cpus jobs of size {0}, and"
                                 " none of that size could be"
                                 " assigned".format(ncpus))
        else:
            if num_jobs != len(newjobs):
                raise ValueError("You requested {0} jobs of size {1} but"
                                 " only {2} could be assigned".format(njobs,
                                                ncpus, len(newjobs)))

        return newjobs
                
    def get_jobs(self):
        return self._job_map_.keys()

    def set_jobs(self, value):
        raise RuntimeError("You can only set the jobs by"
                           " updating the job map")
    def del_jobs(self):
        raise RuntimeError("You can only remove the jobs"
                           " by clearing the job map")

    jobs = property(get_jobs, set_jobs, del_jobs)

    def get_underutilized(self):
        return len(self.unassigned) > 0
    
    def set_underutilized(self, value):
        raise RuntimeError("You cannot set this property")

    def del_underutilized(self):
        raise RuntimeError("You cannot delete this property")

    underutilized = property(get_underutilized, set_underutilized,
                             del_underutilized)

    def create_procs(self, job_list, cmd, cmd_args=[],
                     subdir_prefix=None, outfile_prefix=None,
                     only_local_mem=False):
        """
        This method takes a list of jobs---which must each already be in
        the job map, or a ValueError is raised---an executable, and
        possibly common arguments to that executable, and starts one
        instance of that command for each job in the job list, binding it
        to the corresponding cpus as specified in the job map.

        If 'subdir_prefix' is not 'None', then each job will be invoked in
        its own subdirectory of the current working directory, where the
        directories will be named 'subdir_prefix_job<n>'. This option should
        always be provided if the job will create any output files relative
        to its current working directory. Additionally, if 'outfile_prefix'
        is given, then stdout and stderr will be redirected to a file name
        'outfile_prefix_job<n>' within the job subdirectory; otherwise, they
        will be redirected to /dev/null. It is an error to specify a non-null
        outfile_prefix while leaving 'subdir_prefix' as 'None'.

        If 'only_local_mem' is True, then each job will be started specifying
        that its memory should only come from its local NUMA node. This is only
        possible if the manager was cosntructed with 'numactl' as the manager.

        This method returns two lists: one of the subprocess.Popen instances
        created, and which the caller is completely responsible for monitoring
        and managing from that point forward, and another (possibly empty) list
        of any output files which were opened, and which the caller should
        therefore ensure are closed when the processes exit.
        """

        if not cmd.startswith('/'):
            raise ValueError("Must specify full path to command {0}".format(cmd))

        if self._manager_exec == 'numactl':
            if only_local_mem:
                after_cmd = ['-l', '--']
            else:
                after_cmd = ['--']
        else:
            if only_local_mem:
                raise ValueError("You requested 'only_local_mem' which is only"
                                 " supported with the 'numactl' manager")
            else:
                after_cmd = []

        if (subdir_prefix is None) and  (outfile_prefix is not None):
            raise ValueError("Cannot have empty 'subdir_prefix' if"
                             " 'outfile_prefix' is not empty")

        for job in job_list:
            if job not in self.jobs:
                raise ValueError("Job {0} has not been setup in a"
                                 " previous call to update_job_plan."
                                 " Cannot start process".format(job))

        procs = []
        files = []
        basedir = os.getcwd()

        for job in job_list:
            cmd_line = [self._manager_exec_path] + \
                       [self._manager_arg+self._job_map_[job]] + \
                       after_cmd
            cmd_line += [cmd]
            cmd_line += cmd_args

            if subdir_prefix is not None:
                dirname=os.path.join(basedir, subdir_prefix+'_job'+str(job))
                if not os.path.isdir(dirname):
                    os.mkdir(dirname)
                if outfile_prefix is not None:
                    outname = os.path.join(dirname, outfile_prefix+'_job'+str(job))
                    f = open(outname, "wt", encoding='utf-8')
                    files.append(f)
                    g = subprocess.STDOUT
                else:
                    f = subprocess.DEVNULL
                    g = subprocess.DEVNULL
                p = subprocess.Popen(cmd_line, cwd=dirname, stdin=None,
                                     stdout=f, stderr=g)
                procs.append(p)
            else:
                p = subprocess.Popen(cmd_line)
                proces.append(p)
                
        return procs, files        
