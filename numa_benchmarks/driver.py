#!/usr/bin/env python3
#
# Copyright 2022 California Institute of Technology
#
# You should have received a copy of the licensing terms for this
# software included in the file “LICENSE” located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE.txt

import argparse
from .utils import add_numa_options, verify_numa_options
from .utils import from_cli, handle_unassigned, watch_procs
import sys

def main(args=None):
    if args is None:
        args = sys.argv[1:]

    parser = argparse.ArgumentParser(
        description='Run specified program on each core of a machine',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    add_numa_options(parser)
    opts = parser.parse_args(args)
    verify_numa_options(opts, parser)

    node_manager = from_cli(opts)
    jobs = node_manager.update_job_map(cpus_per_job=opts.cpus_per_job,
                                       num_jobs = None)
    handle_unassigned(opts, node_manager)
    procs, ofiles = node_manager.create_procs(jobs, cmd=opts.executable,
                                              cmd_args=opts.exec_args,
                                              subdir_prefix=opts.subdir_prefix,
                                              outfile_prefix=opts.outfile_prefix,
                                              only_local_mem=opts.only_local_mem)
    watch_procs(opts, procs)

    for f in ofiles:
        f.close()

if __name__ == "__main__":
    sys.exit(main())
