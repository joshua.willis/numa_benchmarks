#!/usr/bin/env python3
#
# Copyright 2022 California Institute of Technology
#
# You should have received a copy of the licensing terms for this
# software included in the file “LICENSE” located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE.txt

import time
import argparse
from .utils import add_numa_options, verify_numa_options
from .utils import from_cli, handle_unassigned, watch_procs
import sys

def main(args=None):
    if args is None:
        args = sys.argv[1:]

    parser = argparse.ArgumentParser(
        description='Run specified program on each core of a machine',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--busy-executable", type=str,
                        help="Executable to run on all other cpus"
                        " to ensure machine is fully occupied")
    parser.add_argument("--busy-args", type=str, nargs='*',
                        help="Arguments to pass to each invocation"
                        " of the busy work executable", default=[])
    parser.add_argument("--busy-subdir-prefix", type=str,
                        help="Beginning of name of subdirectory for"
                        " each busy job invocation; '_job<id>' will be appended",
                        default="busy_workdir")

    add_numa_options(parser)
    opts = parser.parse_args(args)
    verify_numa_options(opts, parser)

    node_manager = from_cli(opts)
    real_work = node_manager.update_job_map(cpus_per_job=opts.cpus_per_job,
                                            num_jobs = 1)
    busy_work = node_manager.update_job_map(cpus_per_job=opts.cpus_per_job,
                                            num_jobs = None)
    handle_unassigned(opts, node_manager)
    # First, create the busy worker processes
    bprocs, bfiles = node_manager.create_procs(busy_work, cmd=opts.busy_executable,
                                               cmd_args=opts.busy_args,
                                               subdir_prefix=opts.busy_subdir_prefix,
                                               outfile_prefix=opts.outfile_prefix,
                                               only_local_mem=opts.only_local_mem)
    # Wait for the polling time to let things get going
    # Conceivably could be its own interval in future development
    time.sleep(opts.poll_interval)

    # Now start the real process
    rprocs, rfiles = node_manager.create_procs(real_work, cmd=opts.executable,
                                               cmd_args=opts.exec_args,
                                               subdir_prefix=opts.subdir_prefix,
                                               outfile_prefix=opts.outfile_prefix,
                                               only_local_mem=opts.only_local_mem)
    watch_procs(opts, rprocs)

    for f in rfiles:
        f.close()
    for p in bprocs:
        p.kill()
    for f in bfiles:
        f.close()

if __name__ == "__main__":
    sys.exit(main())
