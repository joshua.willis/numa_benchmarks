# Copyright 2022 California Institute of Technology
#
# You should have received a copy of the licensing terms for this
# software included in the file “LICENSE” located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE.txt

from setuptools import setup, find_packages

setup(
    name="numa_benchmarks",
    version="0.1.0",
    description="Simple Linux-only class for determining cpus attached to each NUMA node",
    packages=find_packages(include=['numa_benchmarks', 'numa_benchmarks.*']),
    python_requires=">=3.7, <4",
    entry_points={
        "console_scripts": [
            "drive_benchmark = numa_benchmarks.driver:main",
            "single_busywork = numa_benchmarks.single:main"]}
)
    
